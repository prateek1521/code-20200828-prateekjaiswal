# code-20200828-prateekjaiswal

# Project Description

Instead of using API for processing lakhs data. I have used s3 events to invoke the lambda. But I have create another API which will calculate BMI and written test cases for the same. I will mention steps for testing processing of data too.

Sample API response:
```
{"heightInCm":175,"weightInKg":75,"calculatedBMI":24.49}
```

## Requirements
1. Please make you have installed Node.Js 12.
2. Your AWS configuration or export accessKey and secretKey as mentioned below.
    export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
    export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
    export AWS_DEFAULT_REGION=<AWS_DEFAULT_REGION>

### Steps for deployment
Please Follow the below mentioned steps.
1. In `/env/prod.json` add aws account number on which you want to deploy.
2. Go inside `cd lambda/layer/node/nodejs` and run `npm i` command.
3. Go inside cd `lambda/assignment` and run `npm i`.

### Testing
To test BMI calculator locally, run below command.
```
npm run test
```

### Deployment
To deploy and test on aws, run below command.
```
npm run deploy
```

### Steps to test Data processing.

Once deployment is completed. One bucket will be created in s3 with name `data-lake-test-prod`. you need to upload the json in the bucket. It will automatically invoke the lambda process the data. Also, after processing in the same bucket results will stored. Once processing is completed then you see a folder in same bucket with name `Results` go inside and you will find a json file. Inside json you will see 3 objects.
1. Successfully processed data with key `results`.
2. You will see total number of OverWeights.
3. You will see invalidData array.

Note: for processing lambda allocated memory is 3 GB and timeout is 15 mins.

Sample request to tested deployed API on AWS (API is open. No authentication is needed).
https://b8x6444bha.execute-api.ap-south-1.amazonaws.com/calculate/bmi?height=175&weight=75.
Note: First call may not work as I haven't added warm ups to this lambda.


### Observation

As per the task we are going to process lakhs of data, then using API for the same will not be good choice. We can use using s3 events which will process the data json data upload on S3 automatically.

