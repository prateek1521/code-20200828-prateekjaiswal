const BaseHandler = require('common/basehandler');
const utils = require('common/utils');
const S3Manager = require('common/s3Manager');
const _ = require('lodash');

const bmiCategory = "Over weight";
const s3UploadResultPrefix = 'Results';
const BMI_CATEGORY_HEALTH_RISK_MAP = {
    1: {
        bmiCategory: "Under weight",
        healthRisk: "Malnutrition risk"
    },
    2: {
        bmiCategory: "Normal weight",
        healthRisk: "Low risk"
    },
    3: {
        bmiCategory: "Over weight",
        healthRisk: "Enhanced risk"
    },
    4: {
        bmiCategory: "Moderately obese",
        healthRisk: "Medium risk"
    },
    5: {
        bmiCategory: "Severely obese",
        healthRisk: "High risk"
    },
    6: {
        bmiCategory: "Very Severely obese",
        healthRisk: "Very high risk"
    }
};

class DataProcessor extends BaseHandler {

    constructor() {
        super();
    }

    getValidatedParams(event) {
      console.log('getValidatedParams');
        return {error: null, validatedParams: event};
    }

    calculateBMI({HeightCm, WeightKg}) {
        // converting height into meter
        const heightM2 = utils.centimeterToMeter2(HeightCm);
        return utils.calculateBMI(WeightKg, heightM2);
    }

    getBMIIndex(bmi) {
        if(bmi <= 18.4) {
            return 1;
        } else if(bmi >= 18.5 && bmi <= 24.9) {
            return 2;
        } else if(bmi >= 25 && bmi <= 29.9) {
            return 3;
        } else if (bmi >= 30 && bmi <= 34.9) {
            return 4;
        } else if (bmi >= 35 && bmi <= 39.9) {
            return 5;
        } else {
            return 6;
        }
    }

    countTotalOverWeights(results) {
        return (results || []).reduce((counter, item) => {
            counter = item.bmiCategory === bmiCategory ? counter + 1 : counter;
            return counter;
        }, 0);
    }

    async process(event, context, callback) {
        // extracting bucket name
        const bucket = event.Records[0].s3.bucket.name;
        // extracting file name
        const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
        const params = {
            Bucket: bucket,
            Key: key,
        };
        // getting data from s3 bucket for processing
        const data = await S3Manager.readS3File(params);
        // console.log('data', data);
        if (data && Array.isArray(data)) {
            const [validData = [], invalidData = []] = _.partition(data, record => record.HeightCm > 0 && record.WeightKg > 0);
            const results = (validData || []).map(item => {
                const bmi = this.calculateBMI(item);
                const index = this.getBMIIndex(bmi);
                item.calculatedBMI = bmi;
                item = {...item, ...BMI_CATEGORY_HEALTH_RISK_MAP[index]};
                return item;
            });
            const totalOverWeights = this.countTotalOverWeights(results);
            const processedData = {
                totalOverWeights,
                results,
                invalidData
            };
            const filePath = `${s3UploadResultPrefix}/results-${new Date().toISOString()}.json`;
            await S3Manager.writeFile(bucket, filePath, processedData);
            // console.log('data results', results);
            // console.log('invalidData', invalidData);
        } else {
            console.log('data must be in json array');
            return "processing failed";
        }
        return 'processing success';
    }
    
}

exports.main = async(event, context, callback) => {
    return await new DataProcessor().handler(event, context, callback);
}