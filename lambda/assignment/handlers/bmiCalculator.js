const BaseHandler = require('common/basehandler');
const responseHandler = require('common/responseHandler');
const utils = require('common/utils');
const Joi = require('joi');

const numberRegex = new RegExp('^[1-9][0-9]*$');

class CalculateBMI extends BaseHandler {

    constructor() {
        super();
    }

    async getValidatedParams(event) {
        const body = event.queryStringParameters || {};
        const schema = Joi.object({
            height: Joi.string().pattern(numberRegex).required(),
            weight: Joi.string().pattern(numberRegex).required()
        });
        
        const {error} = schema.validate(body)
        console.log('getValidatedParams:', body, error);
        return {error, validatedParams: event};
    }

    async process(event, context, callback) {
        const queryParams = event.queryStringParameters;
        const heightInCm = Number(queryParams.height);
        const weightInKg = Number(queryParams.weight);
        // converting height into meter
        const heightM2 = utils.centimeterToMeter2(heightInCm);
        const bmi = utils.calculateBMI(weightInKg, heightM2);
        return responseHandler.callbackRespondWithJsonBody(200, { heightInCm, weightInKg, calculatedBMI: Number(bmi) });
    }
}

exports.main = async(event, context, callback) => {
    return await new CalculateBMI().handler(event, context, callback);
}