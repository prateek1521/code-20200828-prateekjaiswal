'use strict';

const mocha = require('serverless-mocha-plugin');

const wrapper = mocha.getWrapper('CalculateBMI', '/handlers/bmiCalculator.js', 'main');
const expect = mocha.chai.expect;
describe('CalculateBMI', () => {

	it('This will calculate BMI', async () => {
		const response = await wrapper.run({
			queryStringParameters: {weight: "75", height: "175"}
		});
		// expected status code is 200
		expect(response.statusCode).to.be.equal(200);

		const body = JSON.parse(response.body);
		expect(body.calculatedBMI).to.be.closeTo(24.49, 0.01);
	});

	it(`This should give error as weight can't be 0`, async () => {
		const response = await wrapper.run({
			queryStringParameters: {weight: "0", height: "75"}
		});
		// API will fail as input has to be number string.
		expect(response.statusCode).to.be.equal(400);

		const body = JSON.parse(response.body);
		expect(body.message).to.not.be.empty;
	});

	it('This will give error as input is missing', async () => {
		const response = await wrapper.run({
			queryStringParameters: {weight: "1"}
		});
		// API will failed in case of missing input
		expect(response.statusCode).to.be.equal(400);

		const body = JSON.parse(response.body);
		expect(body.message).to.not.be.empty;
	});

	it('This will throw error as negative number is not allowed', async () => {
		const response = await wrapper.run({
			queryStringParameters: {weight: "175", height: "-10"}
		});

		expect(response.statusCode).to.be.equal(400);

		const body = JSON.parse(response.body);
		expect(body.message).to.not.be.empty;
	});

	it('should error on bad input', async () => {
		const response = await wrapper.run({
			queryStringParameters: {weight: "1", height: "hello"}
		});
		// API will fail as input has to be number string.
		expect(response.statusCode).to.be.equal(400);

		const body = JSON.parse(response.body);
		expect(body.message).to.not.be.empty;
	});

});