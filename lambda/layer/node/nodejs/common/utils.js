
/**
 * 
 * @param {Number} cmValue 
 * @returns 
 */
const centimeterToMeter2 = (cmValue) => {
    const m2 = cmValue/100;
    return m2 * m2;
}

/**
 * 
 * @param {Number} weightInKg 
 * @param {Number} heightInM 
 * @returns 
 */

const calculateBMI = (weightInKg, heightInM) => {
    return (weightInKg / heightInM).toFixed(2)
}

module.exports = {
    centimeterToMeter2,
    calculateBMI
}