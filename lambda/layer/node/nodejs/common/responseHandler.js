'use strict';

exports.callbackRespondWithCodeOnly = (code) => {
    return {
        statusCode: code,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Headers': '*'
        }
    };
};

exports.callbackRespondWithSimpleMessage = (code, message) => {
    return {
        statusCode: code,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Headers': '*'
        },
        body: JSON.stringify({
            message
        })
    };
};

exports.callbackRespondWithJsonBody = (code, body, customHeader = null) => {

    let header = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Headers': '*'
    };

    header = { ...header, ...customHeader };
    return {
        statusCode: code,
        headers: header,
        body: JSON.stringify(body)
    };
};

exports.callbackRespondWithCustomHeader = (code, customHeader = null) => {

    let header = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Headers': '*'
    };

    header = { ...header, ...customHeader };
    return {
        statusCode: code,
        headers: header
    };
};
