const AWS = require('aws-sdk');

const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    region: process.env.REGION
});
    
const writeFile = async(bucketName, fileName, data) => {
    try {
        const params = {
            Bucket: bucketName,
            Key: fileName,
            Body: JSON.stringify(data),
            ContentType: "application/json ; charset=utf-8"
        }
        await s3.putObject(params).promise();
        console.log('uploaded json on s3 successfully')
    }
    catch (error) {
        console.error(`putObject: error occurred while writing to S3: logging error: ${error}`);
    }
}

const readS3File = async(params) => {
    console.log('params:', params);
    try {
        const content = await s3.getObject(params).promise();
        return JSON.parse(content.Body.toString('utf-8'));

    } catch (err) {
        console.log('error readS3File:', err);
        const message = `Error getting object ${params.key} from bucket ${params.bucket}. Make sure they exist and your bucket is in the same region as this function.`;
        throw new Error(message);
    }
}

module.exports = {
    readS3File,
    writeFile
}