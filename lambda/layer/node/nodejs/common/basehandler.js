/* eslint-disable require-await */
const rh = require('./responseHandler');

module.exports = class BaseHandler {

    constructor() {}

    // function to be overridden in subclass
    getValidatedParams(event) {
        console.log('the lambda handler class needs getValidatedParams');
        return {error: null, validatedParams: event};
    }

    // function to be overridden in subclass
    async process(event, context, callback) {
        console.log('base class function');
    };

    // instantiating base with subclass tag
    async handler(event, context, callback) {
        try {
            console.log('Inside BaseHandler', JSON.stringify(event));
            const {error, validatedParams} = await this.getValidatedParams(event);
            if (error) {
                return rh.callbackRespondWithSimpleMessage(400, error);  
            }

            // calling process function of class instantiated
            const response = await this.process(validatedParams, context, callback);
            if (response) {
                console.log('Process complete!!!');
            }
            return response;
        } catch (e) {
            console.error(e);
            if ('statusCode' in e && e.statusCode) {
                return rh.callbackRespondWithSimpleMessage(e.statusCode, e.message);
            }
            return rh.callbackRespondWithSimpleMessage(500, 'Internal Server error');
        } finally {
            // if we have used any database then we can close connection here.
        }
    }
};
